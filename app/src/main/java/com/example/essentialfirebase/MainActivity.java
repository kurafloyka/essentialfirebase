package com.example.essentialfirebase;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {

    FirebaseDatabase database;
    DatabaseReference reference;
    EditText key, value;
    Button add;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        define();
        showResult();
        addValue();

    }


    public void define() {
        database = FirebaseDatabase.getInstance();
        key = findViewById(R.id.key);
        value = findViewById(R.id.value);
        result = findViewById(R.id.result);
        add = findViewById(R.id.add);

    }

    public void addValue() {
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String keyText = key.getText().toString();
                reference = database.getReference(keyText);
                String valueText = value.getText().toString();
                reference.setValue(valueText);

                key.setText("");
                value.setText("");

            }
        });
    }

    public void showResult() {

        reference = database.getReference("name");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                result.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}

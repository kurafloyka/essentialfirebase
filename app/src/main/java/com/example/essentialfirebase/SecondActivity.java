package com.example.essentialfirebase;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class SecondActivity extends AppCompatActivity {


    FirebaseDatabase database;
    DatabaseReference reference, id, name, surName, age, worker;
    List<User> userList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        define();
        readMainKey("farukakyol");
    }

    public void define() {

        database = FirebaseDatabase.getInstance();
        reference = database.getReference("farukakyol");

        reference.setValue("asdfgh");

        id = reference.child("id");
        id.setValue("1");

        name = reference.child("name");
        name.setValue("Faruk");

        surName = reference.child("surName");
        surName.setValue("AKYOL");

        age = reference.child("age");
        age.setValue(28);

        worker = reference.child("worker");
        worker.setValue(true);
    }


    public void readMainKey(String mainKey) {


        reference = database.getReference(mainKey);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                User user = dataSnapshot.getValue(User.class);
                userList = new ArrayList<>();


                Log.i("name : ", dataSnapshot.getValue().toString());
                userList.add(user);

                if (user.isWorker()) {
                    Log.i("Name", "Working....");
                } else {
                    Log.i("Name", "Not working...");
                }

                Log.i("name", " " + user.isWorker());
                System.out.println("test" + user);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


       /* reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                Log.i("Result : ", dataSnapshot.getValue().toString());
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });*/ //WE CAN GET DATA AS FIELD.
    }

}

package com.example.essentialfirebase;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class ThirdActivity extends AppCompatActivity {

    FirebaseDatabase database;
    DatabaseReference reference, reference2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        database = FirebaseDatabase.getInstance();
        reference = database.getReference("Users/faruk");
        reference2 = database.getReference("Users/fatih");

        UserDetails userDetails = new UserDetails("faruk", "akyol", "28");
        reference.setValue(userDetails);


        Map map = new HashMap();
        map.put("city", "Hamburg");
        map.put("country", "Germany");
        map.put("Id", "0000000");
        reference2.setValue(map);


    }
}

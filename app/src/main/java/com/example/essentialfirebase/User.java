package com.example.essentialfirebase;

import androidx.annotation.NonNull;

import com.google.firebase.database.FirebaseDatabase;

public class User {

    private String id, name, surName;
    private boolean worker;
    private int age;

    public User(String surName, boolean worker, String name, String id, int age) {
        this.id = id;
        this.name = name;
        this.surName = surName;
        this.worker = worker;
        this.age = age;
    }

    public User() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public boolean isWorker() {
        return worker;
    }

    public void setWorker(boolean worker) {
        this.worker = worker;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", surName='" + surName + '\'' +
                ", isWorker=" + worker +
                ", age=" + age +
                '}';
    }
}

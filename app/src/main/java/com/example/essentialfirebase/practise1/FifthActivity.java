package com.example.essentialfirebase.practise1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.essentialfirebase.R;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FifthActivity extends AppCompatActivity {

    FirebaseDatabase database;
    EditText password, age, name;
    Button saveButton;
    DatabaseReference reference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fifth);

        name = findViewById(R.id.name);
        password = findViewById(R.id.password);
        age = findViewById(R.id.age);
        saveButton = findViewById(R.id.saveButton);
        database = FirebaseDatabase.getInstance();


        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reference = database.getReference();

                reference = database.getReference("users/" + User.getUserName());
                UserDetails u1 = new UserDetails(password.getText().toString(), age.getText().toString(), name.getText().toString());
                reference.setValue(u1);
                Intent intent = new Intent(FifthActivity.this, SixthActivity.class);
                startActivity(intent);
            }
        });
    }
}

package com.example.essentialfirebase.practise1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.essentialfirebase.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SixthActivity extends AppCompatActivity {

    TextView ageText, nameText, passwordText;
    FirebaseDatabase database;
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sixth);


        database = FirebaseDatabase.getInstance();
        reference = database.getReference("users/" + User.getUserName());

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.i("Data : ", dataSnapshot.getValue().toString());

                UserInfo info = dataSnapshot.getValue(UserInfo.class);
                ageText.setText(info.getAge());
                nameText.setText(info.getName());
                passwordText.setText(info.getPassword());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        ageText = findViewById(R.id.ageText);
        nameText = findViewById(R.id.nameText);
        passwordText = findViewById(R.id.passwordText);


    }
}

package com.example.essentialfirebase.practise1;

public class User {

 private static String userName;

    public static String getUserName() {
        return userName;
    }

    public static void setUserName(String userName) {
        User.userName = userName;
    }
}

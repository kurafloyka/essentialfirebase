package com.example.essentialfirebase.practise1;

public class UserInfo {

    public String password, name, age;

    public UserInfo(String password, String name, String age) {
        this.password = password;
        this.name = name;
        this.age = age;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public UserInfo() {
    }
}

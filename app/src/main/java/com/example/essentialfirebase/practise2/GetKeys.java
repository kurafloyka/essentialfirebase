package com.example.essentialfirebase.practise2;

import com.google.firebase.database.DatabaseReference;

public class GetKeys {


    public static String getKey(DatabaseReference reference) {
        DatabaseReference push = reference.child("message").push();
        String key = push.getKey();
        return key;
    }

}

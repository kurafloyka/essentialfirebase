package com.example.essentialfirebase.practise2;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.essentialfirebase.R;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class UniqueKeyActivity extends AppCompatActivity {

    FirebaseDatabase database;
    DatabaseReference reference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unique_key);

        database = FirebaseDatabase.getInstance();
        reference = database.getReference();

        //specify Main child name
        String mainChild = "message/";

        // produce key
        DatabaseReference push = reference.child("message").push();
        String key = push.getKey();
        // Log.i("Key : ", key);

        //    Log.i("Key2 :", GetKeys.getKey(reference));

        //keye ait cocugun key ve value larin belirlenmesi
        Map detail = new HashMap();
        detail.put("detail", "hello");
        detail.put("date", "03.02.1992");


        Map add = new HashMap();
        add.put(mainChild + key, detail);

        reference.updateChildren(add, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

                Toast.makeText(getApplicationContext(), "Added successfully...", Toast.LENGTH_LONG).show();
            }
        });


    }
}

package com.example.essentialfirebase.practise3;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.essentialfirebase.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UniqueKey2Activity extends AppCompatActivity {

    EditText name, age, number, team;
    Button addButton;
    FirebaseDatabase database;
    DatabaseReference reference;
    String mainChild, key;
    List<User> list;
    RecyclerView recyclerView;
    UserAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unique_key2);

        define();
        addValues();
        loadData();
    }


    public void define() {
        list = new ArrayList<>();
        name = findViewById(R.id.name);
        age = findViewById(R.id.age);
        number = findViewById(R.id.number);
        team = findViewById(R.id.team);
        addButton = findViewById(R.id.addButton);
        database = FirebaseDatabase.getInstance();
        reference = database.getReference();
        mainChild = "Users/";
        recyclerView = findViewById(R.id.recyclerview);

        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
        adapter = new UserAdapter(list, getApplicationContext());
        recyclerView.setAdapter(adapter);

    }


    public void addValues() {

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference reference1 = reference.child("Users").push();
                key = reference1.getKey();
                Map mp = setMessage(name.getText().toString(), age.getText()
                        .toString(), number.getText().toString(), team.getText().toString());


                Map mp2 = new HashMap();
                mp2.put(mainChild + key, mp);
                reference.updateChildren(mp2, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

                        name.setText("");
                        age.setText("");
                        number.setText("");
                        team.setText("");
                        Toast.makeText(getApplicationContext(), "Added successfully...", Toast.LENGTH_LONG).show();


                    }
                });

            }
        });
    }

    public Map setMessage(String name, String age, String number, String team) {

        Map map = new HashMap();
        map.put("name", name);
        map.put("age", age);
        map.put("number", number);
        map.put("team", team);

        return map;
    }


    public void loadData() {


        reference.child("Users").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {


                //Log.i("getData", dataSnapshot.toString());


                User u = dataSnapshot.getValue(User.class);
                adapter.notifyDataSetChanged();
                Log.i("getData", u.toString());
                list.add(u);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}

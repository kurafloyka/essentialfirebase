package com.example.essentialfirebase.practise3;

public class User {

    public String number, name, team, age;


    public User(String number, String name, String team, String age) {
        this.number = number;
        this.name = name;
        this.team = team;
        this.age = age;
    }

    public User() {


    }

    @Override
    public String toString() {
        return "User{" +
                "number='" + number + '\'' +
                ", name='" + name + '\'' +
                ", team='" + team + '\'' +
                ", age='" + age + '\'' +
                '}';
    }
}

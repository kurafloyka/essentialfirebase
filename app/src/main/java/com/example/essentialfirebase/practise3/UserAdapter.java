package com.example.essentialfirebase.practise3;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.essentialfirebase.R;

import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    public List<User> list;
    public Context context;

    public UserAdapter(List<User> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public UserAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.userlayout, parent, false);
        return new UserAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final UserAdapter.ViewHolder holder, int position) {

        holder.numberText.setText(list.get(position).number);
        holder.teamText.setText(list.get(position).team);
        holder.ageText.setText(list.get(position).age);
        holder.nameText.setText(list.get(position).name);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Team : " + holder.teamText.getText().toString(), Toast.LENGTH_LONG).show();
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        //recyclerview ait layout viewlarin tanimlanmasi islemleri yapiliyordu.
        TextView nameText, ageText, teamText, numberText;
        LinearLayout mainLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nameText = itemView.findViewById(R.id.nameText);
            ageText = itemView.findViewById(R.id.ageText);
            teamText = itemView.findViewById(R.id.teamText);
            numberText = itemView.findViewById(R.id.numberText);
            mainLayout = itemView.findViewById(R.id.mainLayout);

        }
    }
}
